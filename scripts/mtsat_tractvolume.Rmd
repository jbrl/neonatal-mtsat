---
title: "`r subdir_name`"
author: "Kadi Vaher"
date: "`r Sys.time()`"
output: html_document
---

```{r setup, include=T, message=F}

rm(list = ls())
gc()
subdir_name <- "mtsat_tractvolume"

# load packages
library(tidyverse)
library(magrittr)
library(here)
library(glue)
library(umx)
library(MVN)
library(rstatix)
library(scales)
library(readxl)
library(knitr)
library("car")
library(lmtest)

# set paths
knitr::opts_knit$set(root.dir = ".", aliases = c(h = "fig.height", w = "fig.width", ow = "out.width"))
knitr::opts_chunk$set(
  dev = c("png"),
  fig.path = here("results", "figures", glue("{subdir_name}/")), dpi = 300
)
theme_set(theme_light()) # global option for ggplot2

```

```{r knit, echo=F, eval=FALSE}

# Run this section at the very end

rmarkdown::render(
  input = here("scripts", str_c(subdir_name, ".Rmd")),
  output_file = here("results", "reports", paste(subdir_name, ".html", sep="_"))
)
```

# READ DATA

```{r read_demo}

demo_data <- read.csv(here("processed_data", "demo_105_clean_25012023.csv"))

```

## Read in tract volumes file

```{r read_metricdata}

#WM ROIs
volumes <- read.csv(here("raw_data", "WM_tract_volumes.csv"), header = T)


```


# Wrangle data

```{r wrangle_data}

demo_data <- demo_data %>% select(record_id, preterm, GA_birth, GA_mri)

#only keep the _volume columns in the tract volumes table
volumes <- volumes %>% select(record_id, contains("_volume"))

#remove the _volume
colnames(volumes) <- str_replace_all(colnames(volumes), "_volume", "")

#-----------------------------------
#merge with demo data

all_data <- merge(demo_data, volumes, by="record_id")


```

# Regression

```{r regressions}

fit <- lm(cbind(scale(all_data[5:ncol(all_data)])) ~ as.factor(preterm) + scale(GA_mri), data = all_data)
sumfit <- summary(fit)

print(sumfit)

Manova(fit)

```

# Regression assumptions
Because plot.mlm is not implemented yet, I will print the model assumptions for each of the models separately here

```{r assumptions}

for (i in 5:ncol(all_data)) {
  print(names(all_data[i]))
  tmp_fit <- lm(scale(all_data[[i]]) ~ as.factor(preterm) + scale(GA_mri), data = all_data)
  
  #print model diagnostic plots and tests for normality and heterogeneity
  print(shapiro.test(tmp_fit[['residuals']]))
  print(bptest(tmp_fit))
  par(mfrow = c(2, 2))
  plot(tmp_fit)
  
}

```

# Write results into a table

```{r write_results_table}

results_table <- data.frame(matrix(ncol = 7, nrow = 0))
x <- c("ROI", "preterm_beta", "preterm_se","preterm_pval", "GAmri_beta", "GAmri_se","GAmri_pval")
colnames(results_table) <- x

for (i in 1:length(sumfit)) {
  name <- names(sumfit[i])
  
  tmp <- data.frame(matrix(ncol = 7, nrow = 1))
  y <- c("ROI", "preterm_beta", "preterm_se","preterm_pval", "GAmri_beta", "GAmri_se","GAmri_pval")
  colnames(tmp) <- y
  
  tmp$ROI <- name
  tmp$preterm_beta <- sumfit[[i]]$coefficients[2,1]
  tmp$GAmri_beta <- sumfit[[i]]$coefficients[3,1]
  tmp$preterm_pval <- sumfit[[i]]$coefficients[2,4]
  tmp$GAmri_pval <- sumfit[[i]]$coefficients[3,4]
  tmp$preterm_se <- sumfit[[i]]$coefficients[2,2]
  tmp$GAmri_se <- sumfit[[i]]$coefficients[3,2]
  
  results_table <- rbind(results_table, tmp)
}

#p-value correction
results_table$preterm_adjp <- p.adjust(results_table$preterm_pval, method="fdr")
results_table$GA_mri_adjp <- p.adjust(results_table$GAmri_pval, method="fdr")

```

# Wrangle the results table: rename columns etc

```{r wrangle_results_table}

results_table <- results_table %>% separate(ROI, into = c("response", "Tract"), sep=" ", remove=F) %>%
  select(-response, -ROI)

write.csv(results_table, here("results","tract_volumes_regressionresults_29052023.csv"), row.names=F)

```
