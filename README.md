# Characterisation of the neonatal brain using myelin-sensitive magnetisation transfer imaging

Code for the analyses presented in: 

> Blesa Cábez, M.#, Vaher, K.#, York, E.N., Galdi, P., Sullivan, G., Stoye, D.Q., Hall, J., Corrigan, A.E., Quigley, A.J., Waldman, A.D , Bastin, M.E., Thrippleton, M.J., Boardman, J.P., affiliations, A., 2023. **Characterisation of the neonatal brain using myelin-sensitive magnetisation transfer imaging**. Imaging Neurosci. doi: https://doi.org/10.1162/imag_a_00017. #co-first authors

Repository author: Kadi Vaher (kadi.vaher@ed.ac.uk)

The project assumes the following file directory system:
- .Rproj file
- scripts
- raw_data
- processed_data
- mtsat_maps_masks
    - study_MTSat
        - subject
            - maps
            - masks
    - voxel_data
    - plots
- qc
- results
    - figures
    - reports
    - plots
    - label_colours

The repository consists of the following folder:

- scripts/ containing R markdown/script files for creating the demographic/clinical variable descriptive tables, performing statistical analyses and creating accompanying figures and tables.
Note: when there are multiple scrips with a similar name, the latest version is the one with "_updated" at the end of the filename.

Requests for anonymised data will be considered under the study's Data Access and Collaboration policy and governance process (https://www.tebc.ed.ac.uk/2019/12/data-access-and-collaboration/). 

The ENA50 neonatal template is available at: https://git.ecdf.ed.ac.uk/jbrl/ena

The scripts to generate the white matter tracts can be found here: https://git.ecdf.ed.ac.uk/jbrl/neonatal-gfactors
